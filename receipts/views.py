from django.shortcuts import redirect
from receipts.models import Account, Receipt, ExpenseCategory
from django.views.generic.list import ListView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic.edit import CreateView
from django.urls import reverse_lazy


# Create your views here.


class ReceiptsView(LoginRequiredMixin, ListView):
    model = Receipt
    template_name = "receipts.html"
    context_object_name = "receipts"

    def get_queryset(self):
        return Receipt.objects.filter(purchaser=self.request.user)


class CreateReceipt(LoginRequiredMixin, CreateView):
    model = Receipt
    fields = [
        "vendor",
        "total",
        "tax",
        "date",
        "category",
        "account",
    ]
    template_name = "create_receipt.html"
    context_object_name = "createlist"
    success_url = reverse_lazy("home")

    def form_valid(self, form):
        item = form.save(commit=False)
        item.purchaser = self.request.user
        item.save()
        return redirect("home")


class ExpenseCategoryListView(LoginRequiredMixin, ListView):
    model = ExpenseCategory
    context_object_name = "expense_category_list"
    template_name = "expense_category_list.html"

    def get_queryset(self):
        return ExpenseCategory.objects.filter(owner=self.request.user)


class AccountListView(LoginRequiredMixin, ListView):
    model = Account
    context_object_name = "accounts_list"
    template_name = "accounts_list.html"

    def get_queryset(self):
        return Account.objects.filter(owner=self.request.user)


class CategoryCreateView(LoginRequiredMixin, CreateView):
    model = ExpenseCategory
    fields = [
        "name",
    ]
    template_name = "create_category.html"
    context_object_name = "create_category"
    success_url = reverse_lazy("list_categories")

    def form_valid(self, form):
        item = form.save(commit=False)
        item.owner = self.request.user
        item.save()
        return redirect("list_categories")


class AccountCreateView(LoginRequiredMixin, CreateView):
    model = Account
    context_object_name = "create_account"
    template_name = "create_account.html"
    success_url = reverse_lazy("accounts_list")
    fields = [
        "name",
        "number",
    ]

    def form_valid(self, form):
        item = form.save(commit=False)
        item.owner = self.request.user
        item.save()
        return redirect("accounts_list")
